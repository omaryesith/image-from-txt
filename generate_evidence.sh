#!/bin/bash

#TODO: Translate all the strings to english

#Define by default values
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
OUTPUT_PATH="$SCRIPTPATH/output" #if not specified, the evidence will be generated at the current path
log_file="$SCRIPTPATH/log/generate_evidence.log"
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

usage="$(basename "$0") [-h][-s][-l][-e][-o][-q][-i] -e <evidence_escript>-- ejecuta el script remoto y genera una salida como una imágen.

Parameters:
     -h Show this help
     -s Hostname or ip of the remote server
     -l Username with which you will connect to the server
        (If it is not passed by default, the user of the current session ($USER) will be used)
     -e The name of the script that generates the evidence
        (Must be in ./scripts)
     -o The exit route where the evidences will be written
        (If it is not passed, the evidence will be written in the "output" directory)
     -q Quiet mode. It will not ask for confirmation before overwriting in case evidence already exists.
     -i The name of the SSH key to be used to connect to the server
        (If it is not passed, it will try to connect with the current user's key (~/.ssh/id_rsa))
"

##TODO: Make a really smart and useful log file
echo "START at $(date)" > $log_file

#Read command line arguments
options_found=0
quiet_mode=0
SSH_KEY='~/.ssh/id_rsa' 
user=$USER
while getopts ':s:e:i:o:l:-q-h' option; do
  options_found=1
  case "${option}" in
    s) host=${OPTARG};;
    e) EVIDENCE_SCRIPT=${OPTARG};;
    i) SSH_KEY=${OPTARG};;
    l) user=${OPTARG};;
    o) OUTPUT_PATH=${OPTARG};;
    q) quiet_mode=1;;
    h) echo "$usage"
       exit
       ;;
    :) printf "\nLa opción -%s requiere argumentos\n\n" "$OPTARG" >&2
        echo "$usage" >&2
        exit 1
        ;;
    \?) printf "\n-%s no es una opción válida\n" "$OPTARG" >&2
        echo "$usage" >&2
        exit 1
        ;;
    * ) echo "$usage" >&2
        exit 1;;
  esac
done
#Validate if any argument was received
if ((!options_found)); then
  echo -e "\nNo se pasaron los parámetros requeridos...\n" >&2
  echo "$usage" >&2
  exit 1
else
  #Let's check if all have value
  if [ -z "$EVIDENCE_SCRIPT" ]; then
    printf "\nLa opción -s es requerida\n\n" >&2
    echo "$usage" >&2
    exit 1
  fi
  if [ ! -e $SSH_KEY ]; then
    printf "\nNo especificó una llave ssh y el usuario actual($USER) no tiene una por defecto.\n\n" >&2
    echo "$usage" >&2
    exit 1
  fi
  if [ -z "$host" ]; then
    printf "\nLa opción -s es requerida.  Debe especificar el host del que requiere la evidencia\n\n" >&2
    echo "$usage" >&2
    exit 1
  fi
  if [ ! -d $OUTPUT_PATH ]; then
    printf "\n\t- ERROR: No es posible generar la evidencia.\n\t  La ruta que ha especificado con -o [$OUTPUT_PATH] no existe, no es posible generar la evidencia\n\n" >&2
    exit 1
  fi
fi

clear
echo -e "\n###### Generando evidencia para el servidor: [$host] ######\n"

echo -en "\t====> Intentando hacer un ping al servidor ... "
this_host_name=$(getent hosts $host | awk '{print $2}')
ping -q -c1 $host &>/dev/null
if [ "$?" == "0" ]; then
    echo -e "$this_host_name [UP]\n"
    host_up=true
else
    echo -e " $this_host_name [DOWN]\n"
    host_up=false
fi

if ((!quiet_mode)); then
  if [ -d $OUTPUT_PATH/.$host ]; then
    echo -e "Ya existe un directorio [$OUTPUT_PATH/.$host] ... "
    echo -e "\t Si continúa se sobreescribiran los ficheros de evidencia exstentes.\n"
    while true; do
      read -p "¿Desea continuar? (S/N) " overwrite_dir
      case $overwrite_dir in
        [Ss]* ) echo -e "\n"; break ;;
        [Nn]* ) echo -e "Saliendo..."; exit 1;;
        * ) echo "Debe responder S o N.";;
      esac
    done
  fi
fi

if [ ! -d $OUTPUT_PATH/$(date +%d%m%Y_%H) ]; then
  mkdir $OUTPUT_PATH/$(date +%d%m%Y_%H)
fi

if [ ! -d $OUTPUT_PATH/.$host ]; then
  mkdir $OUTPUT_PATH/.$host
fi


if $host_up ; then
  #Copy the script file to the host
  echo -en "\t====> Copiando el script [$EVIDENCE_SCRIPT] al servidor $host... "
  scp -i $SSH_KEY $EVIDENCE_SCRIPT $user@$host:/tmp/tmp_script.sh &>>$log_file
  if [ $? -eq 0 ]; then
      echo -e "Ok"
  else
      echo -e "\n\n\tERROR: Ocurrió un error, por favor revise el log [$log_file]\n"
      exit 1
  fi

  #Validate if the script has been copied correctly and execute it
  echo -en "\t====> Ejecutando el script [$EVIDENCE_SCRIPT] en el servidor $host... "
  ssh -i $SSH_KEY -t $user@$host '
  if [ -e /tmp/tmp_script.sh ]; then
    chmod +x /tmp/tmp_script.sh
    /tmp/tmp_script.sh
    rm -f /tmp/tmp_script.sh
  fi
  ' > $OUTPUT_PATH/.$host/$host.txt 2>>$log_file
  if [ $? -eq 0 ]; then
      echo -e "Ok"
  else
      echo -e "\n\n\tERROR: Ocurrió un error, por favor revise el log [$log_file]\n"
      exit 1
  fi
else 
cat > $OUTPUT_PATH/.$host/$host.txt <<EOF
text 5,5 "
\$ date
$(date)
\$ ping -c4 $host
$(ping -c4 $host)
"
EOF
fi

#Generate the image to attach to the excel file
echo -en "\t====> Generando la imagen para la evidencia del servidor ... ";
#calculate the text dimensions
text_width=$(wc -L $OUTPUT_PATH/.$host/$host.txt | cut -d' ' -f1);
text_height=$(wc -l $OUTPUT_PATH/.$host/$host.txt | cut -d' ' -f1)
#calculate the image size
image_width=$(expr $text_width \* 8  + 6 )
image_height=$( expr $text_height \* 15 - 4 )
#generate the image
convert -size "$image_width"x"$image_height" xc:white -font "Roboto-Mono" -pointsize 14 -fill black -draw @$OUTPUT_PATH/.$host/$host.txt $OUTPUT_PATH/$(date +%d%m%Y_%H)/$host$(date +_%d%m%Y_%H%M%S).png &>>$log_file
if [ $? -eq 0 ]; then
    echo -e "Ok"
    if [ -d $OUTPUT_PATH/.$host ]; then
      rm -rf $OUTPUT_PATH/.$host
    fi
else
    echo -e "\n\n\tERROR: Ocurrió un error, por favor revise el log [$log_file]\n"
    if [ -d $OUTPUT_PATH/.$host ]; then
      rm -rf $OUTPUT_PATH/.$host
    fi
    exit 1
fi

echo -e "\t\tFinalizado!\n"
echo -e "#########################################################################\n\n"

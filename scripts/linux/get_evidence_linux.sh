#!/bin/bash

#Temporal file name to use for evidence
OUTPUT_FILE=mktemp

cat > $OUTPUT_FILE <<EOF
text 5,5 "
[root@$HOSTNAME ~]# hostname
$(hostname)
[root@$HOSTNAME ~]# date
$(date)
[root@$HOSTNAME ~]# uptime
$(uptime)
"
EOF

#Dump the content of evidence file to stout using cat
cat $OUTPUT_FILE

#Let's be polit and delete the temporal file
if [ -e $OUTPUT_FILE ]; then
  rm $OUTPUT_FILE
fi

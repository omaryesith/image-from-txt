#!/usr/bin/env bash

for i in $(cat hosts.txt | grep -Ev '^#|^$'); do
  ./generate_evidence.sh -i ~/.ssh/id_rsa -l omar -s $i -e ./scripts/linux/get_evidence_linux.sh -q
done
